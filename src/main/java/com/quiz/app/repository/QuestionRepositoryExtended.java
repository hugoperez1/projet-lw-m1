package com.quiz.app.repository;

import com.quiz.app.domain.Question;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepositoryExtended extends QuestionRepository {

        List<Question> findAllByQuizIdOrderByNb(Long quizId);
}
