package com.quiz.app.repository;

import com.quiz.app.domain.Quiz;
import com.quiz.app.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepositoryExtended extends QuizRepository {

        Page<Quiz> findAllByUser(User user, Pageable pageable);

    @Query("SELECT q  from Quiz q " +
        " WHERE ( :term IS NULL OR (q.title LIKE %:term%)" +
        "AND q.show = true)")
        Page<Quiz> findByFilter(@Param("term")String searchTerm, Pageable pageable);
}
