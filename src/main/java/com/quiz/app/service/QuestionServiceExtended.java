package com.quiz.app.service;

import com.quiz.app.domain.Question;
import com.quiz.app.repository.QuestionRepository;
import com.quiz.app.repository.QuestionRepositoryExtended;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing {@link Question}.
 */
@Service
@Transactional
public class QuestionServiceExtended extends QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionServiceExtended.class);

    private final QuestionRepository questionRepository;
    private final QuestionRepositoryExtended questionRepositoryExtended;


    public QuestionServiceExtended(QuestionRepository questionRepository,
                                   QuestionRepositoryExtended questionRepositoryExtended) {
        super(questionRepository);
        this.questionRepository = questionRepository;
        this.questionRepositoryExtended = questionRepositoryExtended;
    }

    public List<Question> findAllWithQuiz(Long quizId) {
        log.debug("Request to get all Questions for id quiz {}", quizId);
        return questionRepositoryExtended.findAllByQuizIdOrderByNb(quizId);
    }
}
