package com.quiz.app.service.dto;

import com.quiz.app.domain.Question;
import com.quiz.app.domain.Quiz;

import java.util.List;

public class QuizDTO {
    private Quiz quiz;

    private List<Question> questions;

    public QuizDTO() {
        // Empty constructor needed for Jackson.
    }

    public QuizDTO(Quiz quiz, List<Question> questions) {
        this.quiz = quiz;
        this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }
}
