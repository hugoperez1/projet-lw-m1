package com.quiz.app.service;

import com.quiz.app.domain.Question;
import com.quiz.app.domain.Quiz;
import com.quiz.app.domain.User;
import com.quiz.app.repository.QuizRepository;
import com.quiz.app.repository.QuizRepositoryExtended;
import com.quiz.app.service.dto.QuizDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Quiz}.
 */
@Service
@Transactional
public class QuizServiceExtended extends QuizService {

    private final Logger log = LoggerFactory.getLogger(QuizServiceExtended.class);

    private final QuizRepository quizRepository;
    private final QuizRepositoryExtended quizRepositoryExtended;
    private final QuestionServiceExtended questionServiceExtended;


    public QuizServiceExtended(QuizRepository quizRepository,
                               QuizRepositoryExtended quizRepositoryExtended,
                               QuestionServiceExtended questionServiceExtended) {
        super(quizRepository);
        this.quizRepository = quizRepository;
        this.quizRepositoryExtended = quizRepositoryExtended;
         this.questionServiceExtended = questionServiceExtended;
    }

    public Quiz updateStats(Quiz quiz, Integer score) {
        log.debug("Request to update quiz {} with score {}", quiz,  score);
        int nbTries = quiz.getNbTries() == null ? 0 : quiz.getNbTries();
        Integer nbPoints = quiz.getNbPoints() == null ? 0 : quiz.getNbPoints();
        quiz.setNbTries(nbTries + 1);
        quiz.setNbPoints(nbPoints + score);

        return save(quiz);
    }

    public QuizDTO updateQuiz(Quiz quiz, List<Question> questionList) {
        log.debug("Request to save quiz {} and all questions {} from quiz ", quiz, questionList);
        QuizDTO quizDto = new QuizDTO();
       quizDto.setQuestions(
           questionList.stream().map(questionServiceExtended::save).collect(Collectors.toList())
       );

        quizDto.setQuiz(
            save(quiz)
        );
        return quizDto;
    }

    public QuizDTO createNewQuiz(Quiz quiz, List<Question> questionList, User user) {
        log.debug("Request to save quiz {} and all questions {} from quiz ", quiz, questionList);
        quiz.setUser(user);
        Quiz quizSave = save(quiz);
        QuizDTO quizDto = new QuizDTO();
        quizDto.setQuiz(quizSave);

        quizDto.setQuestions(
            questionList.stream().map(question -> {
                question.setQuiz(quizSave);
                return questionServiceExtended.save(question);
            }).collect(Collectors.toList())
        );

        return quizDto;
    }

    public Page<Quiz> findAllByUser(User user, Pageable pageable) {
        log.debug("Request to get all Quiz for user {} with pageable {}", user, pageable);
        return quizRepositoryExtended.findAllByUser(user, pageable);
    }

    public Page<Quiz> findAllWithSearchTerm(String searchTerm, Pageable pageable) {
        log.debug("Request to get all Quiz with searchTerm {} with pageable {}", searchTerm, pageable);
        return quizRepositoryExtended.findByFilter(searchTerm, pageable);
    }

    public void deleteQuiz(Long id) {
        questionServiceExtended
            .findAllWithQuiz(id)
            .forEach(question -> questionServiceExtended.delete(question.getId()));

        delete(id);
    }


}
