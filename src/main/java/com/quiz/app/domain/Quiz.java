package com.quiz.app.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Quiz.
 */
@Entity
@Table(name = "quiz")
public class Quiz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "title", length = 50, nullable = false)
    private String title;

    @Column(name = "jhi_show")
    private Boolean show;

    @Min(value = 0)
    @Column(name = "nb_points")
    private Integer nbPoints;

    @Min(value = 0)
    @Column(name = "nb_tries")
    private Integer nbTries;

    @ManyToOne
    @JsonIgnoreProperties("quizzes")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Quiz title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isShow() {
        return show;
    }

    public Quiz show(Boolean show) {
        this.show = show;
        return this;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Integer getNbPoints() {
        return nbPoints;
    }

    public Quiz nbPoints(Integer nbPoints) {
        this.nbPoints = nbPoints;
        return this;
    }

    public void setNbPoints(Integer nbPoints) {
        this.nbPoints = nbPoints;
    }

    public Integer getNbTries() {
        return nbTries;
    }

    public Quiz nbTries(Integer nbTries) {
        this.nbTries = nbTries;
        return this;
    }

    public void setNbTries(Integer nbTries) {
        this.nbTries = nbTries;
    }

    public User getUser() {
        return user;
    }

    public Quiz user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quiz)) {
            return false;
        }
        return id != null && id.equals(((Quiz) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Quiz{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", show='" + isShow() + "'" +
            ", nbPoints=" + getNbPoints() +
            ", nbTries=" + getNbTries() +
            "}";
    }
}
