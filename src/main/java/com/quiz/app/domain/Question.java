package com.quiz.app.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Question.
 */
@Entity
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(value = 0)
    @Max(value = 9)
    @Column(name = "nb", nullable = false)
    private Integer nb;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "answer", nullable = false)
    private String answer;

    @NotNull
    @Column(name = "fake_1", nullable = false)
    private String fake1;

    @Column(name = "fake_2")
    private String fake2;

    @Column(name = "fake_3")
    private String fake3;

    @ManyToOne(optional = false)
    @NotNull
    private Quiz quiz;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNb() {
        return nb;
    }

    public Question nb(Integer nb) {
        this.nb = nb;
        return this;
    }

    public void setNb(Integer nb) {
        this.nb = nb;
    }

    public String getTitle() {
        return title;
    }

    public Question title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public Question answer(String answer) {
        this.answer = answer;
        return this;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFake1() {
        return fake1;
    }

    public Question fake1(String fake1) {
        this.fake1 = fake1;
        return this;
    }

    public void setFake1(String fake1) {
        this.fake1 = fake1;
    }

    public String getFake2() {
        return fake2;
    }

    public Question fake2(String fake2) {
        this.fake2 = fake2;
        return this;
    }

    public void setFake2(String fake2) {
        this.fake2 = fake2;
    }

    public String getFake3() {
        return fake3;
    }

    public Question fake3(String fake3) {
        this.fake3 = fake3;
        return this;
    }

    public void setFake3(String fake3) {
        this.fake3 = fake3;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public Question quiz(Quiz quiz) {
        this.quiz = quiz;
        return this;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        return id != null && id.equals(((Question) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", nb=" + getNb() +
            ", title='" + getTitle() + "'" +
            ", answer='" + getAnswer() + "'" +
            ", fake1='" + getFake1() + "'" +
            ", fake2='" + getFake2() + "'" +
            ", fake3='" + getFake3() + "'" +
            "}";
    }
}
