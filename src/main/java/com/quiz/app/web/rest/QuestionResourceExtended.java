package com.quiz.app.web.rest;

import com.quiz.app.domain.Question;
import com.quiz.app.domain.Quiz;
import com.quiz.app.service.QuestionService;
import com.quiz.app.service.QuestionServiceExtended;
import com.quiz.app.service.QuizService;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Quiz}.
 */
@RestController
@RequestMapping("/api/v1/question")
public class QuestionResourceExtended extends QuestionResource {

    private final Logger log = LoggerFactory.getLogger(QuestionResourceExtended.class);

    private static final String ENTITY_NAME = "question";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final QuestionService questionService;
    private final QuestionServiceExtended questionServiceExtended;

    public QuestionResourceExtended(QuestionService questionService,
                                    QuestionServiceExtended questionServiceExtended) {
        super(questionService);
        this.questionService = questionService;
        this.questionServiceExtended = questionServiceExtended;
    }

    @GetMapping("/get-all-by-quiz/{quizId}")
    public ResponseEntity<List<Question>> getAllQuestionByQuiz(@PathVariable Long quizId) {
        log.debug("REST request to get list of questions for id quiz {}", quizId);
        if(quizId == null) {
            return ResponseEntity.badRequest().build();
        }
        List<Question> list = this.questionServiceExtended.findAllWithQuiz(quizId);
        return ResponseEntity.ok().body(list);
    }
}
