package com.quiz.app.web.rest;

import com.quiz.app.domain.Authority;
import com.quiz.app.domain.Authority_;
import com.quiz.app.domain.Quiz;
import com.quiz.app.domain.User;
import com.quiz.app.service.QuizService;
import com.quiz.app.service.QuizServiceExtended;
import com.quiz.app.service.UserService;
import com.quiz.app.service.dto.QuizDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

/**
 * REST controller for managing {@link Quiz}.
 */
@RestController
@RequestMapping("/api/v1/quiz")
public class QuizResourceExtended extends QuizResource {

    private final Logger log = LoggerFactory.getLogger(QuizResourceExtended.class);

    private static final String ENTITY_NAME = "quiz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final QuizService quizService;
    private final UserService userService;
    private final QuizServiceExtended quizServiceExtended;

    public QuizResourceExtended(UserService userService,
                                QuizService quizService,
                                QuizServiceExtended quizServiceExtended) {
        super(quizService);
        this.quizService = quizService;
        this.userService = userService;
        this.quizServiceExtended = quizServiceExtended;
    }

    @GetMapping("/get-all")
    public ResponseEntity<Page<Quiz>> getAllQuiz(String searchTerm,  Pageable pageable) {
        log.debug("REST request to get a page of Quiz show with searchTerm {}", searchTerm);
        Page<Quiz> page = quizServiceExtended.findAllWithSearchTerm(searchTerm, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @PostMapping("/new")
    public ResponseEntity<QuizDTO> createNewQuiz(@Valid @RequestBody QuizDTO quizDto) {
        log.debug("REST request to save a new Quiz with questions by quizDTO {}", quizDto);
        Optional<User> maybeUser = userService.getUserWithAuthorities();
        if(!maybeUser.isPresent()) {
            log.error("No user currently logged");
            return ResponseEntity.badRequest().build();
        }
        User user = maybeUser.get();

        QuizDTO quizDtoSave =quizServiceExtended.createNewQuiz(quizDto.getQuiz(), quizDto.getQuestions(), user);

        return ResponseEntity.ok().body(quizDtoSave);
    }

    @PutMapping("")
    public ResponseEntity<QuizDTO> updateQuiz(@Valid @RequestBody QuizDTO quizDto) {
        log.debug("REST request to update Quiz with questions by quizDTO {}", quizDto);
        QuizDTO quizDtoSave = quizServiceExtended.updateQuiz(quizDto.getQuiz(), quizDto.getQuestions());
        return ResponseEntity.ok().body(quizDtoSave);
    }

    @GetMapping("/get-all-by-user")
    public ResponseEntity<Page<Quiz>> getAllQuizByUser(String searchTerm,  Pageable pageable) {
        log.debug("REST request to get a page of Quiz from current user  with searchTerm {}", searchTerm);
        Optional<User> maybeUser = userService.getUserWithAuthorities();
        if(!maybeUser.isPresent()) {
            log.error("No user currently logged");
            return ResponseEntity.badRequest().build();
        }
        User user = maybeUser.get();
        Page<Quiz> page = quizServiceExtended.findAllByUser(user, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Quiz> getQuiz(@PathVariable Long id) {
        log.debug("REST request to get Quiz : {}", id);
        Optional<Quiz> quiz = quizService.findOne(id);
        return ResponseUtil.wrapOrNotFound(quiz);
    }

    @GetMapping("/update-stats")
    public ResponseEntity<Quiz> updateQuizStats(Long id, Integer score) {
        log.debug("REST request to update quiz stat with id : {} and score : {}", id, score);
        Optional<Quiz> maybeQuiz = quizService.findOne(id);

        if(!maybeQuiz.isPresent() || score == null) {
            log.error("In updateQuizStats, no quiz found for id or score is null");
            return ResponseEntity.badRequest().build();
        }

        Quiz quiz = this.quizServiceExtended.updateStats(maybeQuiz.get(), score);
        return ResponseEntity.ok().body(quiz);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuiz(@PathVariable Long id) {
        log.debug("REST request to delete Quiz : {}", id);

        quizServiceExtended.deleteQuiz(id);
        return ResponseEntity.
            noContent()
            .headers(
                HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
