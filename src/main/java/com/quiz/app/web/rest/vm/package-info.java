/**
 * View Models used by Spring MVC REST controllers.
 */
package com.quiz.app.web.rest.vm;
