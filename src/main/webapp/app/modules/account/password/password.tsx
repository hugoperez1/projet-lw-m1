import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Row, Col, Button } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { savePassword, reset } from './password.reducer';

export interface IUserPasswordProps extends StateProps, DispatchProps {}

export const PasswordPage = (props: IUserPasswordProps) => {
  const [password, setPassword] = useState('');

  useEffect(() => {
    props.reset();
    props.getSession();
    return () => props.reset();
  }, []);

  const handleValidSubmit = (event, values) => {
    props.savePassword(values.currentPassword, values.newPassword);
  };

  const updatePassword = event => setPassword(event.target.value);

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="password-title">Changer de mot de passe</h2>
          <AvForm id="password-form" onValidSubmit={handleValidSubmit}>
            <AvField
              name="currentPassword"
              label="Mot de passe actuel"
              placeholder={'Mot de passe actuel'}
              type="password"
              validate={{
                required: { value: true, errorMessage: 'Votre mot de passe est requis.' }
              }}
            />
            <AvField
              name="newPassword"
              label="Nouveau mot de passe"
              placeholder={'Nouveau mot de passe'}
              type="password"
              validate={{
                required: { value: true, errorMessage: 'Votre mot de passe est requis.' },
                minLength: { value: 4, errorMessage: 'Votre mot de passe doit faire au minimum 4 caractères.' },
                maxLength: { value: 50, errorMessage: 'Votre mot de passe doit faire au maximum 50 caractères.' }
              }}
              onChange={updatePassword}
            />
            <PasswordStrengthBar password={password} />
            <AvField
              name="confirmPassword"
              label="Nouveau mot de passe (confirmation)"
              placeholder="Nouveau mot de passe (confirmation)"
              type="password"
              validate={{
                required: {
                  value: true,
                  errorMessage: 'Votre mot de passe est requis.'
                },
                minLength: {
                  value: 4,
                  errorMessage: 'Votre mot de passe doit faire au minimum 4 caractères.'
                },
                maxLength: {
                  value: 50,
                  errorMessage: 'Votre mot de passe doit faire au maximum 50 caractères.'
                },
                match: {
                  value: 'newPassword',
                  errorMessage: 'Les deux mot de passe ne correspondent pas'
                }
              }}
            />
            <Button color="success" type="submit">
              Valider
            </Button>
          </AvForm>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, savePassword, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PasswordPage);
