import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Row, Col, Alert, Button } from 'reactstrap';

import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';
import { IRootState } from 'app/shared/reducers';
import { handleRegister, reset } from './register.reducer';

export type IRegisterProps = DispatchProps;

export const RegisterPage = (props: IRegisterProps) => {
  const [password, setPassword] = useState('');

  useEffect(() => () => props.reset(), []);

  const handleValidSubmit = (event, values) => {
    props.handleRegister(values.username, values.email, values.firstPassword);
    event.preventDefault();
  };

  const updatePassword = event => setPassword(event.target.value);

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1 id="register-title"> Créer un compte </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          <AvForm id="register-form" onValidSubmit={handleValidSubmit}>
            <AvField
              name="username"
              label="Identifiant"
              placeholder={'Votre identifiant'}
              validate={{
                required: { value: true, errorMessage: 'Un identifiant est requis.' },
                pattern: { value: '^[_.@A-Za-z0-9-]*$', errorMessage: 'L\'identifiant ne peut contenir que des chiffres ou des lettres.' },
                minLength: { value: 1, errorMessage: 'L\'identifant doit contenir au moins 1 caractère.' },
                maxLength: { value: 50, errorMessage: 'L\'identifant doit contenir au plus 50 caractères.' }
              }}
            />
            <AvField
              name="email"
              label="Email"
              placeholder={'Votre email'}
              type="email"
              validate={{
                required: { value: true, errorMessage: 'Un email est requis.' },
                minLength: { value: 5, errorMessage: 'L\'email doit contenir au moins 5 caractères.' },
                maxLength: { value: 254, errorMessage: 'L\'email doit contenir au plus 254 caractères.' }
              }}
            />
            <AvField
              name="firstPassword"
              label="Mot de passe"
              placeholder={'Mot de passe'}
              type="password"
              onChange={updatePassword}
              validate={{
                required: { value: true, errorMessage: 'Votre mot de passe est requis.' },
                minLength: { value: 4, errorMessage: 'Le mot de passe doit contenir au moins 4 caractères.'},
                maxLength: { value: 50, errorMessage: 'Le mot de passe doit contenir au plus 50 caractères.' }
              }}
            />
            <PasswordStrengthBar password={password} />
            <AvField
              name="secondPassword"
              label="Confirmation du mot de passe"
              placeholder="Confirmation du mot de passe"
              type="password"
              validate={{
                required: { value: true, errorMessage: 'Votre mot de passe est requis.' },
                minLength: { value: 4, errorMessage: 'Le mot de passe doit contenir au moins 4 caractères.'},
                maxLength: { value: 50, errorMessage: 'Le mot de passe doit contenir au plus 50 caractères.' },
                match: { value: 'firstPassword', errorMessage: 'Les mot de passe sont différents !' }
              }}
            />
            <Button id="register-submit" color="primary" type="submit">
              Créer mon compte
            </Button>
          </AvForm>
        </Col>
      </Row>
    </div>
  );
};

const mapDispatchToProps = { handleRegister, reset };
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  null,
  mapDispatchToProps
)(RegisterPage);
