import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Col, Row, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { getUrlParameter } from 'react-jhipster';
import { RouteComponentProps } from 'react-router-dom';

import { handlePasswordResetFinish, reset } from '../password-reset.reducer';
import PasswordStrengthBar from 'app/shared/layout/password/password-strength-bar';

export interface IPasswordResetFinishProps extends DispatchProps, RouteComponentProps<{ key: string }> {}

export const PasswordResetFinishPage = (props: IPasswordResetFinishProps) => {
  const [password, setPassword] = useState('');
  const [key] = useState(getUrlParameter('key', props.location.search));

  useEffect(() => () => props.reset(), []);

  const handleValidSubmit = (event, values) => props.handlePasswordResetFinish(key, values.newPassword);

  const updatePassword = event => setPassword(event.target.value);

  const getResetForm = () => {
    return (
      <AvForm onValidSubmit={handleValidSubmit}>
        <AvField
          name="newPassword"
          label="Nouveau mot de passe :"
          placeholder={'Nouveau mot de passe'}
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Votre mot de passe est requis.' },
            minLength: { value: 4, errorMessage: 'Votre mot de passe doit faire au minimum 4 caractères.' },
            maxLength: { value: 50, errorMessage: 'Votre mot de passe doit faire au maximum 50 caractères.' }
          }}
          onChange={updatePassword}
        />
        <PasswordStrengthBar password={password} />
        <AvField
          name="confirmPassword"
          label="Nouveau mot de passe (confirmation)"
          placeholder="Nouveau mot de passe (confirmation)"
          type="password"
          validate={{
            required: { value: true, errorMessage: 'Votre mot de passe est requis.' },
            minLength: { value: 4, errorMessage: 'Votre mot de passe doit faire au minimum 4 caractères.' },
            maxLength: { value: 50, errorMessage: 'Votre mot de passe doit faire au maximum 50 caractères.' },
            match: { value: 'newPassword', errorMessage: 'Les deux mot de passe ne sont pas identiques!' }
          }}
        />
        <Button color="success" type="submit">
          Validate new password
        </Button>
      </AvForm>
    );
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="4">
          <h1>Rénitialiser le mot de passe</h1>
          <div>{key ? getResetForm() : null}</div>
        </Col>
      </Row>
    </div>
  );
};

const mapDispatchToProps = { handlePasswordResetFinish, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  null,
  mapDispatchToProps
)(PasswordResetFinishPage);
