import React from 'react';

import { connect } from 'react-redux';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Button, Alert, Col, Row } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { handlePasswordResetInit, reset } from '../password-reset.reducer';

export type IPasswordResetInitProps = DispatchProps;

export class PasswordResetInit extends React.Component<IPasswordResetInitProps> {
  componentWillUnmount() {
    this.props.reset();
  }

  handleValidSubmit = (event, values) => {
    this.props.handlePasswordResetInit(values.email);
    event.preventDefault();
  };

  render() {
    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h1>Réinitialiser votre mot de passe</h1>
            <Alert color="warning">
              <p>Entrez votre adresse mail avec laquelle vous avez créer le compte</p>
            </Alert>
            <AvForm onValidSubmit={this.handleValidSubmit}>
              <AvField
                name="email"
                label="Email: "
                placeholder={'Votre email'}
                type="email"
                validate={{
                  required: { value: true, errorMessage: 'Votre email est requis.' },
                  minLength: { value: 5, errorMessage: 'Votre email doit faire au minimum 5 caractères.' },
                  maxLength: { value: 254, errorMessage: 'Votre email doit faire au maximum 254 caractères.' }
                }}
              />
              <Button color="primary" type="submit">
                Réinitialiser le mot de passe
              </Button>
            </AvForm>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = { handlePasswordResetInit, reset };

type DispatchProps = typeof mapDispatchToProps;

export default connect(
  null,
  mapDispatchToProps
)(PasswordResetInit);
