import React, { useEffect } from 'react';
import { Button, Col, Alert, Row } from 'reactstrap';
import { connect } from 'react-redux';

import { AvForm, AvField } from 'availity-reactstrap-validation';

import { IRootState } from 'app/shared/reducers';
import { getSession } from 'app/shared/reducers/authentication';
import { saveAccountSettings, reset } from './settings.reducer';

export interface IUserSettingsProps extends StateProps, DispatchProps {}

export const SettingsPage = (props: IUserSettingsProps) => {
  useEffect(() => {
    props.getSession();
    return () => {
      props.reset();
    };
  }, []);

  const handleValidSubmit = (event, values) => {
    const account = {
      ...props.account,
      ...values
    };

    props.saveAccountSettings(account);
    event.persist();
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="settings-title">Paramètres de {props.account.login}</h2>
          <AvForm id="settings-form" onValidSubmit={handleValidSubmit}>
            {/* First name */}
            <AvField
              className="form-control"
              name="firstName"
              label="Prénom :"
              id="firstName"
              placeholder="Votre prénom"
              validate={{
                required: { value: true, errorMessage: 'Votre prénom est requis.' },
                minLength: { value: 1, errorMessage: 'Votre prénom doit faire au minimum 1 caractère' },
                maxLength: { value: 50, errorMessage: 'Votre prénom doit faire au maximum 50 caractères' }
              }}
              value={props.account.firstName}
            />
            {/* Last name */}
            <AvField
              className="form-control"
              name="lastName"
              label="Nom :"
              id="lastName"
              placeholder="Votre nom"
              validate={{
                required: { value: true, errorMessage: 'Votre nom est requis.' },
                minLength: { value: 1, errorMessage: 'Votre nom doit faire au minimum 1 caractère' },
                maxLength: { value: 50, errorMessage: 'Votre nom doit faire au maximum 50 caractères' }
              }}
              value={props.account.lastName}
            />
            {/* Email */}
            <AvField
              name="email"
              label="Email :"
              placeholder={'Votre email'}
              type="email"
              validate={{
                required: { value: true, errorMessage: 'Votre email est requis.' },
                minLength: { value: 5, errorMessage: 'Votre email doit faire au minimum 5 caractères' },
                maxLength: { value: 254, errorMessage: 'Votre email doit faire au maximum 254 caractères' }
              }}
              value={props.account.email}
            />
            <Button color="primary" type="submit">
              Valider
            </Button>
          </AvForm>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = { getSession, saveAccountSettings, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPage);
