import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Row, Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter, Col } from 'reactstrap';
import { compose } from 'redux';
import {toast} from "react-toastify";
import {getQuizById, updateQuizStatsById} from "app/shared/services/quiz";
import { getAllByQuiz } from "app/shared/services/question";

const TAB =['answer', 'fake1', 'fake2', 'fake3'];


class QuizList extends Component<any, any> {
  state = {
    quiz: {
      id :null,
      title: "",
      user: null,
      nbPoints: 0,
      nbTries: 0
    },
    questions: [],
    isLoading: true,
    openModal: false,
    questionIndex: 0,
    resultRender: false,
    score: 0
  };

  componentDidMount() {
    const { match } = this.props;
    const { quizId } = match.params;
    this.getQuiz(quizId);
  }

  getQuiz = async quizId => {
    try {
      const result = await getQuizById(quizId);
      const { data } = result;

      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      this.setState({quiz : data}, () => this.loadQuestions());

    } catch (error) {
      toast.error("Impossible de charger le quiz");
      this.setState({isLoading: false });
    }
  };

  loadQuestions = async () => {
    const { quiz } = this.state;
    try {
      const result = await getAllByQuiz(quiz.id);
      const { data } = result;
      this.setState({questions: data}, () => this.randomiseQuestions());
    } catch (e) {
      toast.error("Impossible de charger les questions du quiz");
      this.setState({isLoading: false });
    }
  };

  randomiseQuestions = () => {
    const { questions } = this.state;
    const questionsUpdated = questions.map(question => ({...question, selected: 0, randomise: this.generateRandom() }));
    this.setState({questions: questionsUpdated, isLoading: false});
  };

  generateRandom  = () => {
      const items = [0, 1, 2, 3];
      const random = [];
      for (let i = 0; i < 4; i++) {
        const item = items[Math.floor(Math.random() * items.length)];
        random.push(item);
        items.splice(items.indexOf(item), 1);
      }
      return random;
    };

  setSelected = selected => {
    const { questions, questionIndex } = this.state;
    questions[questionIndex].selected = selected;
    this.setState({questions});
  };

  modalButtonRender = nb => {
    const {questions, questionIndex} = this.state;
    const question = questions[questionIndex];
    return (
      <Col md={6}>
        <Button
          outline={question.selected !== nb}
          color="secondary"
          onClick={() => this.setSelected(nb)}
        >
          {nb + 1}: {question[TAB[question.randomise[nb]]]}
        </Button>
      </Col>
    );
  };
  nextQuestion = () => {
    const {questions, questionIndex} = this.state;
    if (questionIndex < questions.length - 1) {
      this.setState( {questionIndex : questionIndex + 1});
    } else {
      this.getResult();
    }
  };

  getResult = async () => {
    const { questions, quiz } = this.state;
    this.setState({isLoading: true});
     let goodAnswer = 0;
     questions.forEach(question => {
       if (question.randomise[question.selected] === 0) {
         goodAnswer += 1;
       }
     });
     try {
       const result = await updateQuizStatsById(quiz.id, goodAnswer);
       const { data } = result;
       this.setState({ isLoading: false, quiz: data, score : goodAnswer,openModal: false, resultRender: true });

     } catch (e) {
       toast.warn("Une erreur est survenue lors de la mise à jour des stats pour le quiz");
       this.setState({ isLoading: false, score : goodAnswer,openModal: false, resultRender: true });
     }
  };

  modalRender = () => {
    const { openModal, questions, questionIndex } = this.state;
    const question = questions[questionIndex];
    return (
      <div>
        <Modal isOpen={openModal}>
          <ModalHeader>Question {`${questionIndex + 1}/${questions.length}`}</ModalHeader>
          <ModalBody>
            <Row className="justify-content-center"><h2>{question.title}</h2></Row>
            <Row className="justify-content-center mb-3">
              {this.modalButtonRender(0)}
              {this.modalButtonRender(1)}
            </Row>
            <Row className="justify-content-center">
              {this.modalButtonRender(2)}
              {this.modalButtonRender(3)}
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.nextQuestion()}>
              {questionIndex === questions.length - 1 ? 'Voir les résultats' : 'Question suivante'}</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  };

  resultRender = () => {
    const { questions, score } = this.state;
    return (
      <>
        <Row className="justify-content-center">
          <Alert color="light"><h4>Vous avez fait un score de {score}/{questions.length} !</h4></Alert>
        </Row>
        {
          questions.map(question => (question.randomise[question.selected] !== 0) &&
            (<Row  key={question.nb} className="justify-content-center mt-3">
              <Col md={6}>{question.nb + 1}. {question.title}</Col>
              <Col md={3}><p className="text-danger">{question[TAB[question.randomise[question.selected]]]}</p></Col>
              <Col md={3}><p className="text-success">{question.answer}</p></Col>
            </Row>))
        }
        <Row className="justify-content-center mt-4">
          <Link to="/">
           <Button color="info" size="lg">Retourner à l&apos;accueil</Button>
          </Link>
          </Row>
      </>
    );
  };

  render() {
    const {isLoading, quiz, questions, resultRender} = this.state;

    if (isLoading) {
      return (
        <Row className="justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Chargement du quiz...</span>
          </div>
        </Row>
      )
    }

    if (!quiz.id || !questions || questions.length === 0) {
      return (
        <Row className="justify-content-center">
          <Alert color="danger">
            Une erreur est survenue lors du chargement du quiz <Link to="/">retourner à l&apos;accueil</Link>
          </Alert>
        </Row>
      )
    }

    return (
      <>
        <Row className="justify-content-center mb-lg-5">
          <h1>{quiz.title}</h1>
        </Row>

          {resultRender ?
            this.resultRender()
            :
            (
              <Row className="justify-content-center pt-5">
                <Button color="info" size="lg" onClick={() => this.setState({openModal: true})}>Commencer</Button>
              </Row>)
          }
        {this.modalRender()}
      </>
    )
  }
}

export default compose(withRouter)(QuizList);
