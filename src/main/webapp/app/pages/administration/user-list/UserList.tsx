import React, { Component } from 'react';

import { Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { toast } from 'react-toastify';
import { JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import {deleteUser, getAllUsers} from "app/shared/services/users";
import {IRootState} from "app/shared/reducers";
import {connect} from "react-redux";


class UserList extends Component<any, any> {

  state = {
    users: [],
    isLoading: true,
    activePage: 1,
    totalItems : 0,
    openModal: false,
    userToDelete: null
  };

  componentDidMount() {
    this.loadAll();
  }

  loadAll = async () => {
    const { activePage } = this.state;
    try {
      const result = await getAllUsers({
        page: activePage - 1,
        size: ITEMS_PER_PAGE
      });
      const { data } = result;
      this.setState({
        users: data.content,
        isLoading: false,
        totalItems: data.totalElements,
      });
    } catch (error) {
      toast.error("Impossible d'afficher les utilisateurs");
      this.setState({isLoading: false });
    }
  };

  handlePagination = activePage => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.setState({ activePage },() => this.loadAll());
  };


  deleteUser = async () => {
    const { userToDelete } = this.state;
    this.setState({isLoading: true, openModal: false });
    try {
      const result = await deleteUser(userToDelete.login);
      toast.success("Utilisateur supprimé avec succès !");
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      this.setState({isLoading: false, userToDelete: null }, () => this.loadAll());

    } catch (e) {
      toast.error("Impossible de supprimer l'utilisateur");
      this.setState({isLoading: false, userToDelete: null });
    }
  };

  modalRender() {
    const {openModal, userToDelete } = this.state;
    return (
      <div>
        <Modal isOpen={openModal}>
          <ModalHeader>Supprimer l&apos;utilisateur</ModalHeader>
          <ModalBody>
            <Row className="justify-content-center">
             Voulez-vous vraiment supprimer l&apos;utilisateur {userToDelete && userToDelete.login} ?
            </Row>
            <Row className="text-warning mt-3 ml-4">
              Attention ! supprimer l&apos;utilisateur va supprimer ces quiz
            </Row>

          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={() => this.setState({openModal: false, quizToDelete: null})}>
              <FontAwesomeIcon icon="times" /> <span className="d-none d-md-inline">Annuler</span>
            </Button> {' '}
            <Button color="danger" onClick={() => this.deleteUser()}>
              <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Oui</span>
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

  render() {
    const { isLoading, users, activePage, totalItems } = this.state;
    const { account } = this.props;

    if(isLoading) {
      return (
        <Row className="justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Chargement des utilisateurs...</span>
          </div>
        </Row>
      );

    }
    return (
      <>
        <Row className="justify-content-center">
        <h2>Gestion des utilisateurs</h2>
      </Row>
        <Table striped responsive>
          <thead>
          <tr>
            <th>
              <p className="font-weight-bold">
                Login
              </p>
            </th>
            <th>
              <p className="font-weight-bold">
                Nom
              </p>
            </th>
            <th>
              <p className="font-weight-bold">
                Prenom
              </p>
            </th>
            <th>
              <p className="font-weight-bold">
                Mail
              </p>
            </th>
            <th/>
          </tr>
          </thead>
          <tbody>
          {users.map(item => (
            <tr key={item.id}>
              <th scope="row">
                {item && item.login || 'Pas de login'}
              </th>
              <th>
                {item && item.firstName || 'Pas de prénom'}
              </th>
              <th>
                {item && item.lastName || 'Pas de nom'}
              </th>
              <th>
                {item && item.email || 'Pas d\'email'}
              </th>
              <th>
                <Button color="danger"
                        onClick={() => this.setState({openModal: true, userToDelete: item})}
                        disabled={account.login === item.login}>
                  <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Supprimer</span>
                </Button>
              </th>
            </tr>
          ))
          }
          </tbody>
        </Table>

        {this.modalRender()}
        <Row className="justify-content-center">
          <JhiPagination
            activePage={activePage}
            onSelect={this.handlePagination}
            itemsPerPage={ITEMS_PER_PAGE}
            totalItems={totalItems}
            maxButtons={5}/>
        </Row>
      </>
    )
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account
});
export default connect(
  mapStateToProps
)(UserList);
