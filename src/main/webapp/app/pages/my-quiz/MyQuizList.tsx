import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';

import { Row, Alert, Button, Table, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {deleteQuizById, getAllQuizByUser} from 'app/shared/services/quiz';
import { toast } from 'react-toastify';
import { JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {ITEMS_PER_PAGE} from 'app/shared/util/pagination.constants';
import { calcPoint } from 'app/shared/functions';


class MyQuizList extends Component<any, any> {

  state = {
    quiz: [],
    isLoading: true,
    activePage: 1,
    totalItems : 0,
    searchTerm: "",
    quizToDelete: null,
    openModal: false
  };

  componentDidMount() {
    this.loadAll();
  }

  loadAll = async () => {
    const { activePage, searchTerm } = this.state;
    try {
      const result = await getAllQuizByUser({
        page: activePage - 1,
        size: ITEMS_PER_PAGE,
        searchTerm
      });
      const { data } = result;
      this.setState({
        quiz: data.content,
        isLoading: false,
        totalItems: data.totalElements,
      });
    } catch (error) {
      toast.error("Impossible d'afficher les quiz");
      this.setState({isLoading: false });
    }
  };

  handlePagination = activePage => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.setState({ activePage },() => this.loadAll());
  };

  deleteQuiz = async () => {
    const { quizToDelete } = this.state;
    this.setState({isLoading: true, openModal: false });
    try {
      const result = await deleteQuizById(quizToDelete.id);
      toast.success("Quiz supprimé avec succès !");
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      this.setState({isLoading: false, quizToDelete: null }, () => this.loadAll());

    } catch (e) {
      toast.error("Impossible de supprimer le quiz");
      this.setState({isLoading: false, quizToDelete: null });
    }
  };

  modalRender() {
    const {openModal, quizToDelete } = this.state;
    return (
      <div>
        <Modal isOpen={openModal}>
          <ModalHeader>Supprimer le quiz</ModalHeader>
          <ModalBody>
            Voulez-vous vraiment supprimer le quiz {quizToDelete && quizToDelete.title} ?
          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={() => this.setState({openModal: false, quizToDelete: null})}>
              <FontAwesomeIcon icon="times" /> <span className="d-none d-md-inline">Annuler</span>
            </Button> {' '}
            <Button color="danger" onClick={() => this.deleteQuiz()}>
              <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Oui</span>
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
  render() {
    const { isLoading, quiz, activePage, totalItems } = this.state;
    return (
      <>
        <Row className="pb-4">
          <Col md={6}>
            <h2>Mes quiz</h2>
          </Col>
          <Col md={6} className="d-flex justify-content-end pt-1 pr-3">
            <Link to="/create-quiz">
              <Button color="info">
                <FontAwesomeIcon icon="plus" size="lg"/> Créer un nouveau quiz
              </Button>
            </Link>
          </Col>
        </Row>
        { isLoading ?
          (
            <Row className="justify-content-center">
              <div className="spinner-border" role="status">
                <span className="sr-only">Chargement des quiz...</span>
              </div>
            </Row>
          )
          :
          quiz.length ? (
            <>
              <Table striped>
                <thead>
                <tr>
                  <th>
                    <p className="font-weight-bold">
                      Titre
                    </p>
                  </th>
                  <th>
                    <p className="font-weight-bold">
                      Réussite
                    </p>
                  </th>
                  <th>
                    <p className="font-weight-bold">
                      nombre d&apos;éssais
                    </p>
                  </th>
                  <th/>
                  <th/>
                </tr>
                </thead>
                <tbody>
                  {quiz.map(item => (
                    <tr key={item.id}>
                      <th scope="row">
                        {item && item.title || 'Pas de titre'}
                        {item && !item.show && " (Non visible)"}
                      </th>
                      <th>
                        {calcPoint(item)}
                      </th>
                      <th>
                        {item.nbTries}x
                      </th>
                      <th>
                        <Link to={`/edit-quiz/${ item.id }`}>
                          <Button color="info">
                            <FontAwesomeIcon icon="pen" /> <span className="d-none d-md-inline">Modifier</span>
                          </Button>
                        </Link>
                      </th>
                      <th>
                        <Button color="danger"
                                onClick={() => this.setState({openModal: true, quizToDelete: item})}>
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Supprimer</span>
                        </Button>
                      </th>
                    </tr>
                    ))
                  }
                </tbody>
              </Table>
              <Row className="justify-content-center">
                <JhiPagination
                  activePage={activePage}
                  onSelect={this.handlePagination}
                  itemsPerPage={ITEMS_PER_PAGE}
                  totalItems={totalItems}
                  maxButtons={5}/>
              </Row>
            </>
          ) : (
            <Row>
              <Col>
                <Alert color="secondary">
                  <p className="text-center text-muted font-weight-bold">Aucun quiz n&apos;est disponible</p>
                </Alert>
              </Col>
            </Row>
          )
        }
        {this.modalRender()}
      </>
    )
  }
}

export default compose(withRouter)(MyQuizList);
