import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';

import { Row, Alert, Button, Table, Col } from 'reactstrap';
import { getAllQuiz } from 'app/shared/services/quiz';
import { toast } from 'react-toastify';
import { JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { calcPoint } from 'app/shared/functions';


class QuizList extends Component<any, any> {

  state = {
    quiz: [],
    isLoading: true,
    activePage: 1,
    totalItems : 0,
    searchTerm: ""
  };

  componentDidMount() {
    this.loadAll();
  }

  loadAll = async () => {
    const { activePage, searchTerm } = this.state;
    try {
      const result = await getAllQuiz({
        page: activePage - 1,
        size: ITEMS_PER_PAGE,
        searchTerm
      });
      const { data } = result;
      this.setState({
        quiz: data.content,
        isLoading: false,
        totalItems: data.totalElements,
      });
    } catch (error) {
      toast.error("Impossible d'afficher les quiz");
      this.setState({isLoading: false });
    }
  };

  handlePagination = activePage => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.setState({ activePage },() => this.loadAll());
  };

  render() {
    const { isLoading, quiz, activePage, totalItems } = this.state;
    return (
      <>
        <Row className="d-flex justify-content-center">
          <img alt="Quiz" src="content/images/logo.png" className="w-25"/>
        </Row>
        <Row className="d-flex justify-content-center">
          <h2>Trouvez votre quiz</h2>
        </Row>
        { isLoading ?
          (
            <Row className="justify-content-center">
              <div className="spinner-border" role="status">
                <span className="sr-only">Chargement des quiz...</span>
              </div>
            </Row>
          )
          : quiz.length ?
            (
              <>
                <Table striped responsive>
                  <thead>
                    <tr>
                      <th>
                        <p className="font-weight-bold">
                          Titre
                        </p>
                      </th>
                      <th>
                        <p className="font-weight-bold">
                          Créateur
                        </p>
                      </th>
                      <th>
                        <p className="font-weight-bold">
                          Réussite
                        </p>
                      </th>
                      <th/>
                    </tr>
                  </thead>
                  <tbody>
                    {quiz.map(item => (
                      <tr key={item.id}>
                        <th scope="row">
                          {item && item.title || 'Pas de titre'}
                        </th>
                        <th>
                          {item && item.user && item.user.login || 'Pas de nom'}
                        </th>
                        <th>
                          {calcPoint(item)}
                        </th>
                        <th>
                          <Link to={`/quiz/${ item.id }`}>
                            <Button color="info"> <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Jouer !</span></Button>
                          </Link>
                        </th>
                      </tr>
                      ))
                    }
                  </tbody>
                </Table>
                <Row className="justify-content-center">
                  <JhiPagination
                  activePage={activePage}
                  onSelect={this.handlePagination}
                  itemsPerPage={ITEMS_PER_PAGE}
                  totalItems={totalItems}
                  maxButtons={5}/>
                </Row>
              </>
            ) : (
              <Row>
                <Col>
                  <Alert color="secondary">
                    <p className="text-center text-muted font-weight-bold">Aucun quiz n&apos;est disponible</p>
                  </Alert>
                </Col>
              </Row>
            )
        }
      </>
    )
  }
}

export default compose(withRouter)(QuizList);
