import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { Row, Button, Label, Col, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import { AvForm, AvGroup, AvField, AvInput } from 'availity-reactstrap-validation';
import {createQuiz, getQuizById, updateQuiz} from 'app/shared/services/quiz';
import { toast } from 'react-toastify';
import { getAllByQuiz } from 'app/shared/services/question';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NB_QUESTIONS } from 'app/config/constants';

class CreateQuiz extends Component<any, any> {

  state = {
    isLoading: true,
    quiz: {
      id: null,
      title: ""
    },
    questions: [],
    openModal: false,
    questionIndex: 0,
  };

  componentDidMount() {
    const { match } = this.props;
    const { quizId } = match.params;
    quizId ? this.getQuiz(quizId) : this.setState({isLoading : false});
  }

  getQuiz = async quizId => {
    try {
      const result = await getQuizById(quizId);
      const { data } = result;

      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      this.setState({quiz : data}, () => this.loadQuestions());

    } catch (error) {
      toast.error("Impossible de charger le quiz");
      this.setState({isLoading: false });
    }
  };

  loadQuestions = async () => {
    const { quiz } = this.state;
    try {
      const result = await getAllByQuiz(quiz.id);
      const { data } = result;
      this.setState({questions: data, isLoading: false });
    } catch (e) {
      toast.error("Impossible de charger les questions du quiz");
      this.setState({isLoading: false });
    }
  };

  save = async (event, errors, values) => {
    if (errors.length === 0) {
      const {quiz, questions} = this.state;
      const quizUpdate = {...quiz, ...values};
      this.setState({isLoading: true});
      try {
        const result = quiz.id ?
          await updateQuiz(quizUpdate, questions) :
          await createQuiz(quizUpdate, questions);
        const { data } = result;
        this.setState({isLoading: false, quiz: data.quiz, questions: data.questions});
        toast.success("Quiz sauvegardé avec succès !");

      } catch (e) {
        this.setState({isLoading: false});
        toast.error("Une erreur est survenue lors de la sauvegarde du quiz")


      }
    }

  };

  saveQuestion = (event, errors, values) => {
    if(errors.length === 0) {
      const { questions, questionIndex } = this.state;
      this.setState({openModal: false});
      questions[questionIndex] = {...questions[questionIndex], ...values, nb: questionIndex};

      questionIndex === NB_QUESTIONS - 1 ?
        this.setState({questions, openModal: false, questionIndex: 0}) :
        this.setState({questions, openModal: true, questionIndex: questionIndex + 1});
    }
  };

  modalRender = () => {
    const { openModal, questions, questionIndex } = this.state;
    const question = questions[questionIndex] || {};
    if(!openModal) {
      return null;
    }

    return (
      <div>
        <Modal isOpen={openModal}>
          <ModalHeader>Question {`${questionIndex + 1}/${NB_QUESTIONS}`}</ModalHeader>
          <AvForm model={question} onSubmit={this.saveQuestion}>
            <ModalBody>
              <Row>
                <Col>
                  <AvGroup>
                    <Label for="question-title">Intitulé de la question * :</Label>
                    <AvField
                      id="question-title"
                      type="text"
                      name="title"
                      validate={{
                        required: { value: true, errorMessage: 'Ce champ est requis' }
                      }}
                    />
                  </AvGroup>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <AvGroup>
                    <Label for="question-answer">Reponse * :</Label>
                    <AvField
                      id="question-answer"
                      type="text"
                      name="answer"
                      validate={{
                        required: { value: true, errorMessage: 'Ce champ est requis' }
                      }}
                    />
                  </AvGroup>
                </Col>
                <Col md={6}>
                  <AvGroup>
                    <Label for="question-fake1">Fausse 1 * :</Label>
                    <AvField
                      id="question-fake1"
                      type="text"
                      name="fake1"
                      validate={{
                        required: { value: true, errorMessage: 'Ce champ est requis' }
                      }}
                    />
                  </AvGroup>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <AvGroup>
                    <Label for="question-fake2">Fausse 2 :</Label>
                    <AvField
                      id="question-fake2"
                      type="text"
                      name="fake2"
                    />
                  </AvGroup>
                </Col>
                <Col md={6}>
                  <AvGroup>
                    <Label for="question-fake3">Fausse 3 :</Label>
                    <AvField
                      id="question-fake3"
                      type="text"
                      name="fake3"
                    />
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="info" type="submit">
                {questionIndex === NB_QUESTIONS - 1 ? 'Valider' : 'Question suivante'}</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    )
  };

  render() {
    const { isLoading, quiz, questions, openModal } = this.state;
     if (isLoading) {
       return (
         <Row className="justify-content-center">
           <div className="spinner-border" role="status">
             <span className="sr-only">Chargement de la création du quiz...</span>
           </div>
         </Row>
       );
     }

     if(openModal) {
       return (
         <Row className="justify-content-center">
           <div className="spinner-border" role="status">
             <span className="sr-only">Chargement de la création du quiz...</span>
           </div>
           {this.modalRender()}
         </Row>
       )
     }

    return (
      <>
        <Row className="justify-content-center">
            <h2>{quiz.id ? 'Édition' : 'Création' } du quiz</h2>
        </Row>
        <Row className="justify-content-center">
          <Col md={8}>
            <AvForm model={quiz} onSubmit={this.save}>
              {questions.length === NB_QUESTIONS && (
                <Row>
                  <Col md={8}>
                    <AvGroup>
                      <Label id="titleLabel" for="quiz-title">
                        Titre du quiz * :
                      </Label>

                      <AvField
                        id="quiz-title"
                        type="text"
                        name="title"
                        validate={{
                          required: {value: true, errorMessage: 'Ce champ est requis'},
                          maxLength: {value: 50, errorMessage: 'Ce champ ne peut pas dépasser 50 caractères'}
                        }}
                      />
                    </AvGroup>
                  </Col>
                  <Col md={4}>
                    Voir le quiz :
                    <AvGroup className="ml-4 pb-4">
                      <AvInput id="show" type="checkbox" className="form-control" name="show"/>
                    </AvGroup>
                  </Col>
                </Row>)
              }

              <Row>
                <Button color={questions.length === NB_QUESTIONS ? 'info' : 'warning'} outline size="lg" disabled className="ml-3">
                  Nombre de question(s) : {`${questions.length}/${NB_QUESTIONS}`}
                </Button>
                <Button color="info" className="ml-5" onClick={() => this.setState({openModal: true})}>
                  Gérer les questions
                </Button>
              </Row>

              {this.modalRender()}
              <Row className="d-flex justify-content-end">
                <Button color="info" id="quiz-save" type="submit" disabled={questions.length !== NB_QUESTIONS}>
                  <FontAwesomeIcon icon="save" /> <span className="d-none d-md-inline">Sauvegarder</span>
                </Button>
              </Row>
            </AvForm>
          </Col>
        </Row>
      </>
    )
  }
}

export default compose(withRouter)(CreateQuiz);
