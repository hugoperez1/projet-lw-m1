import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Login from 'app/modules/login/login';
import Register from 'app/modules/account/register/register';
import Activate from 'app/modules/account/activate/activate';
import PasswordResetInit from 'app/modules/account/password-reset/init/password-reset-init';
import PasswordResetFinish from 'app/modules/account/password-reset/finish/password-reset-finish';
import Logout from 'app/modules/login/logout';
import QuizList from 'app/pages/home/QuizList';
import PrivateRoute from 'app/shared/auth/private-route';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import { AUTHORITIES } from 'app/config/constants';
import QuizItem from "app/pages/quiz/QuizItem";
import MyQuizList from "app/pages/my-quiz/MyQuizList";
import CreateQuiz from "app/pages/create-quiz/CreateQuiz";
import UserList from "app/pages/administration/user-list/UserList";

const Account = Loadable({
  loader: () => import(/* webpackChunkName: "account" */ 'app/modules/account'),
  loading: () => <div>loading ...</div>
});

const Routes = () => (
  <div className="view-routes">
    <Switch>
      <ErrorBoundaryRoute path="/login" component={Login} />
      <ErrorBoundaryRoute path="/logout" component={Logout} />
      <ErrorBoundaryRoute path="/account/register" component={Register} />
      <ErrorBoundaryRoute path="/account/activate/:key?" component={Activate} />
      <ErrorBoundaryRoute path="/account/reset/request" component={PasswordResetInit} />
      <ErrorBoundaryRoute path="/account/reset/finish/:key?" component={PasswordResetFinish} />
      <PrivateRoute path="/admin/user-list" component={UserList} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
      <PrivateRoute path="/account" component={Account} hasAnyAuthorities={[AUTHORITIES.ADMIN, AUTHORITIES.USER]} />
      <PrivateRoute path="/my-quiz" component={MyQuizList} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <PrivateRoute path="/edit-quiz/:quizId" component={CreateQuiz} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <PrivateRoute path="/create-quiz" component={CreateQuiz} hasAnyAuthorities={[AUTHORITIES.USER]}/>
      <ErrorBoundaryRoute path="/" exact component={QuizList} />
      <ErrorBoundaryRoute path="/quiz/:quizId" component={QuizItem} />
      <ErrorBoundaryRoute component={PageNotFound} />
    </Switch>
  </div>
);

export default Routes;
