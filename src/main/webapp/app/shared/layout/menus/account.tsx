import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink as Link } from 'react-router-dom';

import { NavDropdown } from './menu-components';

const accountMenuItemsAuthenticated = (
  <>
    <MenuItem icon="wrench" to="/account/settings">
      Mon compte
    </MenuItem>
    <MenuItem icon="lock" to="/account/password">
      Mot de passe
    </MenuItem>
    <MenuItem icon="sign-out-alt" to="/logout">
      Se déconnecter
    </MenuItem>
  </>
);

const accountMenuItems = (
  <>
    <MenuItem id="login-item" icon="sign-in-alt" to="/login">
      Se connecter
    </MenuItem>
    <MenuItem icon="sign-in-alt" to="/account/register">
      Créer un compte
    </MenuItem>
  </>
);

export const AccountMenu = ({ isAuthenticated = false }) => (
  <NavDropdown icon="user" name={isAuthenticated? "Mon compte" : "S'identifier"} id="account-menu">
    {isAuthenticated ? accountMenuItemsAuthenticated : accountMenuItems}
  </NavDropdown>
);

export default AccountMenu;
