import './footer.scss';

import React from 'react';

import { Col, Row } from 'reactstrap';

const Footer = props => (
  <div className="footer page-content">
    <Row>
      <Col md="12" className="d-flex flex-row-reverse">
        <p className="text-muted">Site crée par Hugo PEREZ</p>
      </Col>
    </Row>
  </div>
);

export default Footer;
