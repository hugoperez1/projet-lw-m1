import axios from 'axios';
import { SERVER_API_URL } from 'app/config/constants';

const apiUrl = `${SERVER_API_URL}api/quizzes`;
const apiV1Url = `${SERVER_API_URL}api/v1/quiz`;

export const getAllQuiz = ({ searchTerm = '', page = null, size = null, sort = 'title,asc' } = {}) =>
  axios.get(`${apiV1Url}/get-all`, {
    params: {
      searchTerm,
      sort,
      page,
      size,
      cacheBuster: new Date().getTime()
    }
  });

export const createQuiz = (quiz, questions) => axios.post(`${apiV1Url}/new`, { quiz, questions });

export const updateQuiz = (quiz, questions) => axios.put(`${apiV1Url}`, { quiz, questions });

export const getAllQuizByUser = ({ searchTerm = '', page = null, size = null, sort = 'title,asc' } = {}) =>
  axios.get(`${apiV1Url}/get-all-by-user`, {
    params: {
      searchTerm,
      sort,
      page,
      size,
      cacheBuster: new Date().getTime()
    }
  });

export const getQuizById = quizId => axios.get(`${apiV1Url}/${quizId}`, { params: { cacheBuster: new Date().getTime() } });

export const deleteQuizById = quizId => axios.delete(`${apiV1Url}/${quizId}`, { params: { cacheBuster: new Date().getTime() } });

export const updateQuizStatsById = (id, score) =>
  axios.get(`${apiV1Url}/update-stats`, { params: { id, score, cacheBuster: new Date().getTime() } });
