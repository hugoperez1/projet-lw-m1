import axios from 'axios';
import { SERVER_API_URL } from 'app/config/constants';

const apiV1Url = `${SERVER_API_URL}api/v1/question`;

export const getAllByQuiz = quizId => axios.get(`${apiV1Url}/get-all-by-quiz/${quizId}`, { params: { cacheBuster: new Date().getTime() } });
