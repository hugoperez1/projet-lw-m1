import axios from 'axios';
import { SERVER_API_URL } from 'app/config/constants';

const apiUrl = `${SERVER_API_URL}api/users`;

export const getAllUsers = ({ page = null, size = null, sort = 'login,asc' } = {}) =>
  axios.get(`${apiUrl}`, {
    params: {
      sort,
      page,
      size,
      cacheBuster: new Date().getTime()
    }
  });

export const deleteUser = login => axios.delete(`${apiUrl}/${login}`);
