import React from 'react';

export const calcPoint = item => {
  if (item && item.nbPoints && item.nbTries || item.nbTries !== 0) {
    const value = item.nbTries && Math.round((item.nbPoints / item.nbTries) * 10) || 0;

    const className = value > 49 ? "text-success" : value > 24 ? "text-warning" : "text-danger";


    return <p className={className}>{value}%</p>;

  } else {
    return "Pas encore d'essai pour ce quiz"
  }
};
