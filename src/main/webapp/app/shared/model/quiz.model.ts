import { IUser } from 'app/shared/model/user.model';

export interface IQuiz {
  id?: number;
  title?: string;
  show?: boolean;
  nbPoints?: number;
  nbTries?: number;
  user?: IUser;
}

export const defaultValue: Readonly<IQuiz> = {
  show: false
};
