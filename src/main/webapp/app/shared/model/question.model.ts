import { IQuiz } from 'app/shared/model/quiz.model';

export interface IQuestion {
  id?: number;
  nb?: number;
  title?: string;
  answer?: string;
  fake1?: string;
  fake2?: string;
  fake3?: string;
  quiz?: IQuiz;
}

export const defaultValue: Readonly<IQuestion> = {};
